# gll - a gitlab pipeline linter
A naïve wrapper for the
[gitlab lint api](https://docs.gitlab.com/ee/api/lint.html).

`gll` uses the gitlab lint api to lint your projects `.gitlab-ci.yml` file.
It can be ran as a git pre-push hook.

**Since this process requires authentication, a gitlab token must be created &**
**set in the `GITLAB_TOKEN` environment variable.**

## dependencies
- git >= 1.8.2 (for support of pre-push hooks)
- jq
- curl

## install
Simply clone the script, mark as executable, make sure you export a gitlab token
in the `GITLAB_TOKEN` environment variable & run the script.
- `git clone https://gitlab.com/wiggins.jonathan/gll.git`
- `chmod +x gll/gll`
- `export GITLAB_TOKEN=<your super secure gitlab token here>`

## usage
You can either point gll directly to a file you'd like to verify.
- `gll <file>`

or run the script in a git repo that contains a file named `.gitlab-ci.yml`
(such as gll itself).
- `cd gll`
- `gll`

### as a git hook
This script is great as a git pre-push hook that will automatically lint your
gitlab pipeline file before you push!

Want to use this program with your project? Rename `gll` to `pre-push` & put
in the .git/hooks directory of your project.

Every time you run `git init`, `gll` will be put in .git/hooks for your project.
Want to add it to an already created project? Run `git init` in the project
directory. This is a non-destructive process.

Want to use it on every git project you have?
- Create a git template directory that will contain `gll`. Files and directories
  in this directory whose name do not start with a dot will be copied to .git
  after `git init`. Then symlink `gll` into that directory.
  * `mkdir -p ~/.git_template/pre-push.d`
  * `git config --global init.templatedir '~/.git_template'`
  * `ln -s gll ~/.git_template/pre-push.d/gll`

Since git only understands a single pre-push hook named `pre-push`, it is
recommended to call your scripts (like gll) in a master script named `pre-push`.
The creation of this script is left as an exercise to the user.

You can bypass any `pre-push` hooks with `git push --no-verify`
